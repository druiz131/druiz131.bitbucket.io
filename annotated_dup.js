var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", null ]
    ] ],
    [ "closedloop", null, [
      [ "Closed_Loop", "classclosedloop_1_1Closed__Loop.html", "classclosedloop_1_1Closed__Loop" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "motor", null, [
      [ "DRV8847", "classmotor_1_1DRV8847.html", "classmotor_1_1DRV8847" ],
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "TP_closedloop", null, [
      [ "TP_ClosedLoop", "classTP__closedloop_1_1TP__ClosedLoop.html", "classTP__closedloop_1_1TP__ClosedLoop" ]
    ] ],
    [ "TP_imu", null, [
      [ "TP_IMU", "classTP__imu_1_1TP__IMU.html", "classTP__imu_1_1TP__IMU" ]
    ] ],
    [ "TP_motor", null, [
      [ "TP_Motor", "classTP__motor_1_1TP__Motor.html", "classTP__motor_1_1TP__Motor" ]
    ] ],
    [ "TP_touchpanel", null, [
      [ "TP_TouchPanel", "classTP__touchpanel_1_1TP__TouchPanel.html", "classTP__touchpanel_1_1TP__TouchPanel" ]
    ] ]
];