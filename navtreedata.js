/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Ruiz Project", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "LAB 0x01", "page_L1.html", [
      [ "1.OVERVIEW", "page_L1.html#sum_1", null ],
      [ "2.FINITE STATE MACHINES", "page_L1.html#FSM_1", null ],
      [ "3.LED VIDEO", "page_L1.html#vid_1", null ],
      [ "4.SOURCE CODE", "page_L1.html#code_1", null ]
    ] ],
    [ "LAB 0x02", "page_L2.html", [
      [ "1.OVERVIEW", "page_L2.html#sum_2", null ],
      [ "2.FINITE STATE MACHINES", "page_L2.html#FSM_2", null ],
      [ "3.TASK DIAGRAM", "page_L2.html#task_2", null ],
      [ "4.SOURCE CODE", "page_L2.html#code_2", null ]
    ] ],
    [ "LAB 0x03", "Page_L3.html", [
      [ "1.OVERVIEW", "Page_L3.html#sum_3", null ],
      [ "2.FINITE STATE MACHINES", "Page_L3.html#FSM_3", null ],
      [ "3.TASK DIAGRAM", "Page_L3.html#task_3", null ],
      [ "4.SOURCE CODE", "Page_L3.html#code_3", null ]
    ] ],
    [ "LAB 0x04", "page_L4.html", [
      [ "1.OVERVIEW", "page_L4.html#sec_sum4", null ],
      [ "2.TEST RESULTS", "page_L4.html#sec_test", null ],
      [ "3.FINITE STATE MACHINES", "page_L4.html#FSM_4", null ],
      [ "4.TASK DIAGRAM", "page_L4.html#task_4", null ],
      [ "5.BLOCK DIAGRAM", "page_L4.html#blcok_4", null ],
      [ "6.SOURCE CODE", "page_L4.html#code_4", null ]
    ] ],
    [ "HW 0x02", "page_HW2.html", [
      [ "1.SYSTEM LINKAGE KINEMATICS", "page_HW2.html#sec_1", null ],
      [ "2.BALL KINEMATICS", "page_HW2.html#sec_2", null ],
      [ "3.LINKAGE KINETICS", "page_HW2.html#sec_3", null ],
      [ "4.SYSTEM EOMS", "page_HW2.html#sec_4", null ],
      [ "5.SEPERATING TERMS", "page_HW2.html#sec_5", null ],
      [ "6.CREATING MATRICES", "page_HW2.html#sec_6", null ]
    ] ],
    [ "HW 0x03", "page_HW3.html", [
      [ "1.BLOCK DIAGRAMS", "page_HW3.html#sec_21", null ],
      [ "2.SIMULATION MATRICES", "page_HW3.html#sec_22", null ],
      [ "3.OPEN LOOP SIMULATIONS", "page_HW3.html#sec_23", null ],
      [ "4.CLOSED LOOP SIMULATIONS", "page_HW3.html#sec_24", null ]
    ] ],
    [ "TERM PROJECT", "page_TP.html", [
      [ "1.OVERVIEW", "page_TP.html#sec_TP1", null ],
      [ "2.TASK DIAGRAM", "page_TP.html#sec_TP2", null ],
      [ "3.TOUCHPANEL TASK", "page_TP.html#sec_TP3", null ],
      [ "4.IMU TASK", "page_TP.html#sec_TP4", null ],
      [ "5.CONTROLLER TASK", "page_TP.html#sec_TP5", null ],
      [ "6.MOTOR TASK", "page_TP.html#sec_TP6", null ],
      [ "7.DATA TASK", "page_TP.html#sec_TP7", null ],
      [ "8.USER TASK", "page_TP.html#sec_TP8", null ],
      [ "9.DEMONSTRATION VIDEOS", "page_TP.html#sec_TP9", null ],
      [ "10.DATA COLLECTION", "page_TP.html#sec_TP10", null ],
      [ "11.SOURCE CODE", "page_TP.html#sec_TP11", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Page_L3.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';