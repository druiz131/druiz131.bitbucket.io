var files_dup =
[
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "motor.py", "motor_8py.html", [
      [ "motor.DRV8847", "classmotor_1_1DRV8847.html", "classmotor_1_1DRV8847" ],
      [ "motor.Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "TP_closedloop.py", "TP__closedloop_8py.html", [
      [ "TP_closedloop.TP_ClosedLoop", "classTP__closedloop_1_1TP__ClosedLoop.html", "classTP__closedloop_1_1TP__ClosedLoop" ]
    ] ],
    [ "TP_imu.py", "TP__imu_8py.html", [
      [ "TP_imu.TP_IMU", "classTP__imu_1_1TP__IMU.html", "classTP__imu_1_1TP__IMU" ]
    ] ],
    [ "TP_main.py", "TP__main_8py.html", null ],
    [ "TP_motor.py", "TP__motor_8py.html", [
      [ "TP_motor.TP_Motor", "classTP__motor_1_1TP__Motor.html", "classTP__motor_1_1TP__Motor" ]
    ] ],
    [ "TP_touchpanel.py", "TP__touchpanel_8py.html", [
      [ "TP_touchpanel.TP_TouchPanel", "classTP__touchpanel_1_1TP__TouchPanel.html", "classTP__touchpanel_1_1TP__TouchPanel" ]
    ] ]
];